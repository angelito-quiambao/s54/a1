let collection = [];
let newCollection = [];

// Write the queue functions below.

/*
function print() {
    //Print queue elements.
    return collection
}

function enqueue(name){
    //add elements in the array
    collection.push(name)

    return collection
}

function dequeue(){
    //remove first element in the array
    collection.shift()

    return collection
}

function front(){
    //show first element of the array
    return collection[0]
}

function size(){
    //get the size of the array
    return collection.length 
}

function isEmpty(){
    //check if array is empty
    if (collection.length) {
        return false
    }
    else{
        return true
    }
}*/

//SOLUTION 2 for enqueue and dequeue

function print() {
    //Print queue elements.
    return collection
}

function enqueue(name){
    //add elements in the array
    let index = collection.length;
    collection[index] = name;

    return collection
}

function dequeue(){
    newCollection = [];

    //remove first element in the array
    for(let i = 0; i < collection.length; i++){
        newCollection[i] = collection[i+1];
        break;
    }

    collection = newCollection;

    return collection
}


function front(){
    //show first element of the array
    return collection[0]
}


function size(){
    //get the size of the array
    return collection.length
}

function isEmpty(){
    //check if array is empty
    if (collection.length) {
        return false
    }
    else{
        return true
    }
}

module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};